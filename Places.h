#ifndef PLACES_H
#define PLACES_H

#include <vector>
using namespace std;

class Places
{
public:
    Places();

    void readCoordinates( const char* filename );

    const vector< double >& getX() const { return x_; }
    const vector< double >& getY() const { return y_; }
    const vector< vector< double > >& getDistances() const { return distances_; }
    const vector< int > getOrder() const { return order_; }

    void setWay( vector< int >&& way ) { order_ = way; }
    void setWay( const vector< int >& way ) { order_ = way; }
    void randomConnects();
    double calcPathLenght( const vector< int >* way = nullptr ) const;
//    double swapTwoConnectionsByIndexes( int firstIndexToGo, int secondIndexToGo );
//    double rotateConnectionsByIndex( int indexToGo );


    static void invertConnections( vector< int >& order, int beginInvertIndex, int endInvertIndex );
    void invertConnections(int beginInvertIndex, int endInvertIndex );
    // calculate the difference between current summary path length and path length if invert
    double tryToInvertConnections( int beginInvertIndex, int endInvertIndex ) const;


private:
    vector< double > x_;
    vector< double > y_;
    vector< vector< double > > distances_;
    vector< int > order_;
};

#endif // PLACES_H
