#include "GeneticAlgorithm.h"
#include <cmath>
#include <ctime>
#include <algorithm>
using namespace std;

void GeneticAlgorithm::mating( vector<int>& destChild, const vector<int>& parentFirst,
                               const vector<int>& parentSecond, int k )
{
	// bu-ga-ga-ga-ga-ga-ga-ga-ga-ga-ga!!!!

    // first get some parts of genetic from first   
    vector< int >::const_iterator separateIterOfFirst = parentFirst.begin() + k + 1;
    copy( parentFirst.begin(), separateIterOfFirst, destChild.begin() );

    // copy genes from second in its order of permutation
    vector< int >::const_iterator iter = find_first_of( parentSecond.begin(),
                parentSecond.end(), separateIterOfFirst, parentFirst.end() );
    destChild[k+1] = *iter;

    int size = destChild.size();
    for( int j = 2, end = size - k; j < end; ++j )
    {
        // start search from the next after previous found
        iter = find_first_of( iter+1, parentSecond.end(), separateIterOfFirst, parentFirst.end() );
        destChild[j+k] = *iter;
    }

}

//#define DEBUG_STRANGE_CRASHES
#ifdef DEBUG_STRANGE_CRASHES
#include <fstream>
#endif

pair< double, vector< int > > GeneticAlgorithm::doSearch()
{
#ifdef DEBUG_STRANGE_CRASHES
    ofstream logFile;
    logFile.open( "log.txt", ios::out | ios::app );
    logFile << "GeneticAlgorithm::doSearch() begin;" << endl;
#endif

    srand( time( nullptr ) );
    const double mutationProbability = mutationProbability_;

    places_.randomConnects(); // need fot first initializing
    const int size = places_.getOrder().size();
    int iterNum = 0;
//    double sumPath = places_.calcPathLenght();
//    states_.push_back( sumPath );

    double bestPathLength = unsigned(0)-1;
//    double maxPathLength = 0;

    const int m = populationCount_;
    vector< vector< int > > population( m, places_.getOrder() );
    vector< vector< int > > extendedPopulation( 2*m, vector< int >( size ) );
    for ( int i = 0; i < m; ++i )
        random_shuffle( population[i].begin(), population[i].end() ); // first random for m points

//    vector< int > bestPath( places_.getOrder() );

    while ( iterNum++ !=  maxIterCount_ )
    {
        for( int i = 0; i < m; i+=2 )
        {
            // make two children of the two parents
            int k = rand() % size;

            mating( extendedPopulation[i], population[i], population[i+1], k );

            mating( extendedPopulation[i+1], population[i+1], population[i], k );
        }

#ifdef DEBUG_STRANGE_CRASHES
        logFile << "GeneticAlgorithm::doSearch() after matings, iterNum = " << iterNum << endl;
#endif

        for ( int i = 0; i < m; ++i )
        {
            // mutation for created children
            static const double maxRandValue = RAND_MAX;
            if ( rand() / maxRandValue < mutationProbability ) // if it is lucked, do mutation
            {
                int reverseIndex1 = rand() % size;
                int reverseIndex2 = rand() % size;

                if ( reverseIndex1 == reverseIndex2 ) // nothing will be changed
                    continue;

                if ( reverseIndex1 > reverseIndex2 )
                    swap( reverseIndex1, reverseIndex2 );
                reverse( extendedPopulation[i].begin() + reverseIndex1,
                         extendedPopulation[i].begin() + reverseIndex2 + 1 );
            }
        }

#ifdef DEBUG_STRANGE_CRASHES
        logFile << "GeneticAlgorithm::doSearch() after mutations, iterNum = " << iterNum << endl;
#endif

        // copy whole old population to the second half of a extended
        for( int i = 0; i < m; ++i )
            extendedPopulation[ m + i ].swap( population[ i ] );

#ifdef DEBUG_STRANGE_CRASHES
        logFile << "GeneticAlgorithm::doSearch() after swaps, iterNum = " << iterNum << endl;
#endif

        // shuffle whole extendedPopulation and choose the new population
//        random_shuffle( extendedPopulation.begin(), extendedPopulation.end() );
        int newBestWayIndex = -1;
        double currMax = 0;
        for( int i = 0; i < m; ++i )
        {
            double leftWayLength = places_.calcPathLenght( &extendedPopulation[ 2 * i ] );
            double rightWayLength = places_.calcPathLenght( &extendedPopulation[ 2* i + 1 ] );
            if ( leftWayLength < rightWayLength )
            {
                population[ i ].swap( extendedPopulation[ 2 * i ] );
                if ( leftWayLength < bestPathLength ) // check best
                {
                    bestPathLength = leftWayLength;
                    newBestWayIndex = i;
                }

                if ( leftWayLength > currMax )
                    currMax = leftWayLength;
            }
            else
            {
                population[ i ].swap( extendedPopulation[ 2 * i + 1 ] );
                if ( rightWayLength < bestPathLength ) // check best
                {
                    bestPathLength = rightWayLength;
                    newBestWayIndex = i;
                }

                if ( rightWayLength > currMax )
                    currMax = leftWayLength;
            }
        }

        if ( newBestWayIndex != -1 )
            places_.setWay( population[ newBestWayIndex ] );
//            bestPath = population[ newBestWayIndex ];

        max_.push_back( currMax );


#ifdef DEBUG_STRANGE_CRASHES
        logFile << "GeneticAlgorithm::doSearch() after update best, iterNum = " << iterNum << endl;
#endif

        states_.push_back( bestPathLength );

        if ( showProgressFunc_ )
            showProgressFunc_( double( iterNum * 100 ) / maxIterCount_ );

        if ( updateWindowFunc_ /* && iterNum % 100 == 0 */ )
            updateWindowFunc_();
    } // while()

#ifdef DEBUG_STRANGE_CRASHES
        logFile << "GeneticAlgorithm::doSearch() before the last setWay" << endl;
        logFile << "GeneticAlgorithm::doSearch() bestWay: ";
//        for( auto i : bestPath )
//            logFile << i << " ";
        logFile << endl;
#endif

    if ( showProgressFunc_ )
        showProgressFunc_( 100 );
//    places_.setWay( bestPath );

#ifdef DEBUG_STRANGE_CRASHES
    logFile << "GeneticAlgorithm::doSearch() after setWay before return" << endl;
#endif
    return pair< double, vector< int > >( bestPathLength, places_.getOrder() );
}
