#ifndef _MY_READER_FROM_FILE_
#define _MY_READER_FROM_FILE_

#include "FunctionWithTuple.h"

#include <vector>
#include <fstream>
#include <iostream>
#include <tuple>

template < class T >
std::vector<T> readDataFromStream( std::istream& input )
{
    std::vector< T > data;
    T tmp;
    
    while( !input.eof() && input >> tmp && !input.fail() )
        data.push_back( tmp );
        
    return data;
}

template < class T >
std::vector<T> readDataFromFile( const char* filename )
{
    std::ifstream input;
    input.open( filename, std::ios::in );
    if ( !input.good() )
        return std::vector< T >( 0 );
    
    std::vector< T > data( readDataFromStream< T >( input ) ); 
            
    input.close();
    return data;
}


template < class T >
int saveVectorToStream( std::ostream& out, const std::vector< T >& data,
                        const bool newLineForEach = false )
{
    if ( data.empty() )
        return 0;
    
    const char delimiter = newLineForEach ? '\n' : ' ' ;
    for( auto i : data )
        out << i << delimiter;
        
    return data.size();
}

template < class T >
int saveVectorToFile( const char* filename, const std::vector< T >& data,
                        const bool newLineForEach = false )
{
    std::ofstream output;
    output.open( filename, std::ios::out );
    if ( !output.good() )
        return -1;
            
    int result = saveVectorToStream( output, data, newLineForEach );        
        
    output.close();
    return result;
}


template < class Type > 
int saveTableToStream( std::ostream& out, const std::vector< std::tuple< Type > >& table,
                    const char* valueSeparator = " " )
{
    if ( table.empty() )
        return 0;
        
    return 1;
}

#endif
