#ifndef GENETICALGORITHM_H
#define GENETICALGORITHM_H

#include "BaseAlgorithmInterface.h"

class GeneticAlgorithm : public BaseAlgorithmInterface
{
public:
    GeneticAlgorithm( Places& places, int maxIterCount = 10000, ShowProgressFunc func = nullptr,
                      UpdateWindowFunc updFunc = nullptr ) : BaseAlgorithmInterface( places, func, updFunc ),
        maxIterCount_( maxIterCount ), mutationProbability_( 0.05 ), populationCount_( 50 ), max_() {}

    virtual pair< double, vector< int > > doSearch();

    void setMutationProbability( double mutationProbability ) { mutationProbability_ = mutationProbability; }
    void setPopulationCount( int count ) { populationCount_ = count % 2 == 0 ? count : count - 1; }
    const vector< double >& getVectorMax() { return max_; }

private:
    static void mating( vector< int >& destChild, const vector< int >& parentFirst,
            const vector< int >& parentSecond, int k );

private:
    int maxIterCount_;
    double mutationProbability_;
    int populationCount_;
    vector< double > max_;
};

#endif // GENETICALGORITHM_H
