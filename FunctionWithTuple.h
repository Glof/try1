#ifndef _FUNCTION_WITH_TUPLE_
#define _FUNCTION_WITH_TUPLE_

#include <tuple>

namespace {

template < std::size_t i, template < class Arg > class UnaryFunction, class... Tp >
struct UnaryFunctionTupleElement;
 
template < std::size_t i, template < class Arg > class UnaryFunction, class Head, class... Tail >
struct UnaryFunctionTupleElement< i, UnaryFunction, std::tuple< Head, Tail... > > :
    UnaryFunctionTupleElement< i - 1, UnaryFunction, std::tuple< Tail... > > {};
 
template < template < class Arg > class UnaryFunction, class Head, class... Tail >
struct UnaryFunctionTupleElement< 0, UnaryFunction, std::tuple< Head, Tail... > >
{
    static void call( Head& arg )
    {
        UnaryFunction< Head > uf;
        uf( arg );
    }
};
 
template < std::size_t I, std::size_t N, template < class Arg > class UnaryFunction, class... Tp >
struct for_;
 
template < std::size_t N, template < class Arg > class UnaryFunction, class... Tp >
struct for_< N, N, UnaryFunction, std::tuple< Tp... > >
{
    static void call( std::tuple< Tp... >& ) {}
};
 
template < std::size_t I, std::size_t N, template < class Arg > class UnaryFunction, class... Tp >
struct for_< I, N, UnaryFunction, std::tuple< Tp... > >
{
    static void call( std::tuple< Tp... >& t )
    {
        UnaryFunctionTupleElement< I, UnaryFunction, std::tuple< Tp... > >::call( std::get< I >( t ) );
        for_< I + 1, N, UnaryFunction, std::tuple< Tp... > >::call( t );
    }
};

} // namespace
 
template < template < class Arg > class UnaryFunction, class Tuple >
inline void for_each_in_tuple( Tuple& t )
{
    for_< 0, std::tuple_size< Tuple >::value, UnaryFunction, Tuple >::call( t );
}

#endif
 