#ifndef BASEALGORITHMINTERFACE_H
#define BASEALGORITHMINTERFACE_H

#include <vector>
#include <functional>
#include "Places.h"
using namespace std;

//typedef void ( *ShowProgressFunc )( double donePercent );
typedef function< void(double) > ShowProgressFunc;
typedef function< void() > UpdateWindowFunc;

class BaseAlgorithmInterface
{
public:
    virtual ~BaseAlgorithmInterface() {}
    BaseAlgorithmInterface( Places& places, ShowProgressFunc func = nullptr,
                            UpdateWindowFunc updFunc = nullptr ) :
        places_( places ), states_(), showProgressFunc_( func ), updateWindowFunc_( updFunc ) {}

    virtual pair< double, vector< int > > doSearch() = 0;
    const vector< double >& getStates() const { return states_; }
    vector< double >&& releaseStates() { return move( states_ ); }

    ShowProgressFunc getShowProgressFunc() { return showProgressFunc_; }
    void setShowProgressFunc( ShowProgressFunc func ) { showProgressFunc_ = func; }

    UpdateWindowFunc getUpdateWindowFunc() { return updateWindowFunc_; }
    void setUpdateWindowFunc( UpdateWindowFunc func ) { updateWindowFunc_ = func; }

protected:
    Places& places_;
    vector< double > states_;
    ShowProgressFunc showProgressFunc_;
    UpdateWindowFunc updateWindowFunc_;
};

#endif // BASEALGORITHMINTERFACE_H
