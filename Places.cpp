#include "Places.h"
#include "MyDataFileFunctions.h"
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <algorithm>

Places::Places() : x_(), y_(), distances_(), order_()
{
//    srand( time( nullptr ) );
}

void Places::readCoordinates( const char* filename )
{
    ifstream input;
    input.open( filename, ios::in );
    if ( !input.good() )
        return;

    double tmp;
    x_.clear();
    y_.clear();

    while( !input.eof() && input >> tmp && !input.fail() )
    {
        input >> tmp;
        x_.push_back( tmp );
        input >> tmp;
        y_.push_back( tmp );
    }

    input.close();

    int size = x_.size();
    distances_.resize( size );
    for( auto& v : distances_ )
        v.resize( size );

    for( int i = 0; i < size; ++i )
        for( int j = 0; j < size; ++j )
            distances_[i][j] = sqrt( pow( x_[i] - x_[j], 2.0 ) + pow( y_[i] - y_[j], 2.0 ) );
}

void Places::randomConnects()
{
    const int size = x_.size();
    /*
    vector< double > indexes( size );
    for( int i = 0; i < size; ++i )
        indexes[i] = i;

    order_.resize( size );
    int n, index;
    for( int i = 0; i < size; ++i )
    {
        n = size - i;
        index = rand() % n;
        order_[ i ] = indexes[ index ];

        int tmp = indexes[ n-1 ];
        indexes[ n-1 ] = indexes[ index ];
        indexes[ index ] = tmp;
    }
    */
    order_.resize( x_.size() );
    for( int i = 0; i < size; ++i )
        order_[i] = i;

    std::random_shuffle( order_.begin(), order_.end() );
}

double Places::calcPathLenght( const vector< int >* way /* = nullptr */ ) const
{
    const vector< int >& order = way ? *way : order_;
    double pathLength = 0;
    int prevIndex = order[0];
    int nextIndex;
    for( int i = 1, size = order.size(); i < size; ++i )
    {
        nextIndex = order[ i ];
        pathLength += distances_[ prevIndex ][ nextIndex ];
        prevIndex = nextIndex;
    }
    pathLength += distances_[ nextIndex ][ order[ 0 ] ];

    return pathLength;
}

/*
double Places::swapTwoConnectionsByIndexes( int firstIndexToGo, int secondIndexToGo )
{
    if ( firstIndexToGo == secondIndexToGo )
        return pathLength_;

    const int size = order_.size();
    int firstPrevToGo = firstIndexToGo  == 0 ? order_.back() : order_[ firstIndexToGo - 1 ];
    int firstNextToGo = firstIndexToGo  == size-1 ? order_[0] : order_[ firstIndexToGo + 1 ];
    int secondPrevToGo = secondIndexToGo  == 0 ? order_.back() : order_[ secondIndexToGo - 1 ];
    int secondNextToGo = secondIndexToGo  == size-1 ? order_[0] : order_[ secondIndexToGo + 1 ];
    int firstToGo = order_[ firstIndexToGo ];
    int secondToGo = order_[ secondIndexToGo ];

    pathLength_ -= distances_[ firstPrevToGo ][ firstToGo ];
    pathLength_ -= distances_[ firstToGo ][ firstNextToGo ];
    pathLength_ -= distances_[ secondPrevToGo ][ secondToGo ];
    pathLength_ -= distances_[ secondToGo ][ secondNextToGo ];

    int tmp = order_[ firstIndexToGo ];
    order_[ firstIndexToGo ] = order_[ secondIndexToGo ];
    order_[ secondIndexToGo ] = tmp;

    pathLength_ += distances_[ firstPrevToGo ][ secondToGo ];
    pathLength_ += distances_[ secondToGo ][ firstNextToGo ];
    pathLength_ += distances_[ secondPrevToGo ][ firstToGo ];
    pathLength_ += distances_[ firstToGo ][ secondNextToGo ];

    return pathLength_;
}
*/

/*
double Places::rotateConnectionsByIndex( int indexToGo )
{
    const int size = order_.size();
    int indexFromGo = indexToGo == 0 ? size-1 : indexToGo-1;
    int toGo = order_[ indexToGo ];
    int fromGo = indexToGo == 0 ? order_.back() : order_[ indexToGo - 1 ];
    int prevFromGo = indexFromGo == 0 ? order_.back() : order_[ indexFromGo - 1 ];
    int nextToGo = indexToGo == size-1 ? order_[0] : order_[ indexToGo + 1 ];

    pathLength_ -= distances_[ prevFromGo ][ fromGo ];
    pathLength_ -= distances_[ fromGo ][ toGo ];
    pathLength_ -= distances_[ toGo ][ nextToGo ];

    int tmp = order_[ indexToGo ];
    order_[ indexToGo ] = order_[ indexFromGo ];
    order_[ indexFromGo ] = tmp;

    pathLength_ += distances_[ prevFromGo ][ toGo ];
    pathLength_ += distances_[ toGo ][ fromGo ];
    pathLength_ += distances_[ fromGo ][ nextToGo ];

    return pathLength_;
}
*/

double Places::tryToInvertConnections( int beginInvertIndex, int endInvertIndex ) const
{
    if ( beginInvertIndex == endInvertIndex )
        return 0; // reverte nothing

    const int size = order_.size();
    if ( endInvertIndex == size-1 && beginInvertIndex == 0 )
        return 0; // reverte whole way so change nothing because of cyrcle permutation


    endInvertIndex = endInvertIndex % size;
    if ( endInvertIndex < beginInvertIndex )
    {
        int tmpEnd = endInvertIndex;
        endInvertIndex = beginInvertIndex - 1;
        beginInvertIndex = tmpEnd + 1;
    }

    int beginFrom = beginInvertIndex == 0 ? order_.back() : order_[ beginInvertIndex - 1 ];
    int beginTo = order_[ beginInvertIndex ];
    int endFrom = order_[ endInvertIndex ];
    int endTo = endInvertIndex == size-1 ? order_[0] : order_[ endInvertIndex + 1 ];
    return -distances_[ beginFrom ][ beginTo ] - distances_[ endFrom ][ endTo ] +   // before
            distances_[ beginFrom ][ endFrom ] + distances_[ endTo ][ beginTo ];   // after
}

void Places::invertConnections( int beginInvertIndex, int endInvertIndex )
{
    invertConnections( order_, beginInvertIndex, endInvertIndex );
}

void Places::invertConnections( vector<int>& order, int beginInvertIndex, int endInvertIndex )
{
    const int size = order.size();
    endInvertIndex = endInvertIndex % size;
    if ( endInvertIndex < beginInvertIndex )
    {
        int tmpEnd = endInvertIndex;
        endInvertIndex = beginInvertIndex - 1;
        beginInvertIndex = tmpEnd + 1;
    }

    std::reverse( order.begin() + beginInvertIndex, order.begin() + endInvertIndex + 1 );
}
