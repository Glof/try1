#include <iostream>
#include "Places.h"
#include "GeneticAlgorithm.h"
using namespace std;

int main()
{
    Places places;
    places.readCoordinates( "data2.txt" );
    GeneticAlgorithm ga( places, 10 );
    auto result = ga.doSearch();
    
    cout << "Result: distance = " << result.first << ", path: ";
    for ( auto i : result.second )
        cout << i << " ";
    cout << endl;
}